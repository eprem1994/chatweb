<?php

namespace App\Listeners;

use App\Events\MessageTrigger;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class MessageListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MessageTrigger  $event
     * @return void
     */
    public function handle(MessageTrigger $event)
    {
        return $event->args;
    }
}
