<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use Illuminate\Support\Facades\Input;
use App\Events\MessageTrigger;
use \Auth;
use App\Room;
use App\Message;

class ChatWebApp extends Controller
{
	public function index()
	{
		$chatRooms = $this->getChatRoomsData();
		return view('chatHome')->with(['chatRooms' => $chatRooms]);
	}

	public function createChatRoom()
	{
		return view('createChatRoom');
	}

	public function chatRoom($id)
	{
		$currentUserName = $this->getCurrentUserName();
		$currentUserId = $this->getCurrentUserId();
		$room = Room::find($id);
		$title = $room->title;
		$roomImage = 'uploads/'.$room->image;
		$messages = Message::all()->where('room_id', $id);

		$messageViewData = array();
		foreach ($messages as $message) {
			$userName = 'unknown user';

			if (!empty($message->user)) {
				$userName = $message->user->name;
			}
		
			$currentData['userName'] = $userName;
			$currentData['message'] = $message->message;
			$messageViewData[] = $currentData;
		}
		
		return view('chatRoom')->with(
			[
				'messageViewData' => $messageViewData,
				'roomImage' => $roomImage,
				'roomTitle' => $title,
				'currentUserName' => $currentUserName,
				'currentUserId' => $currentUserId,
				'roomId' => $id
			]
		);
	}

	public function message(Request $request)
	{
		if (!$request->ajax()) {
			return '';
		}
		$subissionData = $request->all();

		// save message to db
		$messgae = new Message;
		$messgae->message = $subissionData['message'];
		$messgae->user_id = $subissionData['userId'];
		$messgae->room_id = $subissionData['roomId'];
		$messgae->save();

		broadcast(new MessageTrigger($subissionData));

		return Response::json($subissionData);
	}

	public function saveRoom(Request $request)
	{
		$imageName = '';

		// upload chat room image to the uploads section
		if (Input::hasFile('chatRoomImage')) {
			$chatRoomImageFile = Input::file('chatRoomImage');
			$imageName = $chatRoomImageFile->getClientOriginalName();
			$chatRoomImageFile->move('uploads', $imageName);
		}

		$title = $request->input('chatRoomTitle');
		$showToLoggedInUsers = (bool)$request->input('showToLoggedInUsers');
		$usrId = (int)$this->getCurrentUserId();

		// save room data
		$room = new Room;
		$room->title = $title;
		$room->image = $imageName;
		$room->showToLoggedIn = $showToLoggedInUsers;
		$room->save();

		// redirect to the home page
		return redirect('');
	}

	/**
	 * Get Current user id
	 *
	 * @since 1.0.0
	 *
	 * @return int $userId
	 *
	 */
	private function getCurrentUserId()
	{
		$userId = 0;
		$user = Auth::user();

		if (empty($user)) {
			return $userId;
		}
		$userId = (int)$user->id;

		return $userId;
	}

	/**
	 * Get Current user name
	 *
	 * @since 1.0.0
	 *
	 * @return string $userName
	 *
	 */
	private function getCurrentUserName()
	{
		$userName = 'unknown user';
		$user = Auth::user();

		if (empty($user)) {
			return $userName;
		}
		$userName = $user->name;

		return $userName;
	}

	/**
	 * Get chat rooms data related to user status
	 *
	 * @since 1.0.0
	 *
	 * @return array $roomsData
	 *
	 */
	private function getChatRoomsData()
	{
		$auth = Auth::check();
		$roomsData = array();

		if (Auth::check()) {
			$rooms = Room::all();
		}
		else {
			$rooms = Room::all()->where('showToLoggedIn', 0);
		}

		if (empty($rooms)) {
			return $roomsData;
		}

		foreach ($rooms as $room) {

			if (empty($room)) {
				continue;
			}
			$currentRoomData = [];
			$currentRoomData['roomUrl'] = url('/chatRoom/'.$room->id.'');
			$currentRoomData['title'] = $room->title; 
			$currentRoomData['image'] = 'uploads/'.$room->image; 

			$roomsData[] = $currentRoomData;
		}

		return $roomsData;
	}
}