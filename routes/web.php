<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ChatWebApp@index');
Route::get('/createChatRoom', 'ChatWebApp@createChatRoom');
Route::get('/chatRoom/{id}', 'ChatWebApp@chatRoom');
Route::post('/saveRoom', 'ChatWebApp@saveRoom');
Route::post('/message', 'ChatWebApp@message');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
