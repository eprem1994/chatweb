function Chat() {
	this.init();
}

Chat.prototype.init = function() {
	var sendButton = jQuery('#send-message');

	if (!sendButton.length) {
		return false;
	}
	var that = this;

	sendButton.bind('click', function() {
		var messgaArgs = {};
		messgaArgs.beforeSend = function() {
			jQuery('.send-message').prop('disabled', true);
			jQuery('.send-message').val('Please wait...');
		};
		messgaArgs.message = jQuery('#write-message-section').val();
		messgaArgs.userName = jQuery(this).data('user-name');
		messgaArgs.userId = jQuery(this).data('user-id');
		messgaArgs.roomId = jQuery(this).data('room-id');

		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		jQuery.post('/message', messgaArgs, function(e) {
			jQuery('.send-message').prop('disabled', false);
		});
	})

	Echo.channel('chatMessage').listen('MessageTrigger', function(e) {
		var result = "<div class=\"current-message-wrapper\">";
			result += "<div class=\"message\"><span>"+e.args.message+"</span></div>";
			result += "<div class=\"message-author-name\"><span><b>"+e.args.userName+"</b></span></div>";
			result += "</div>";
			jQuery(".messages-wrapper").append(result);
			jQuery('#write-message-section').val('');
	});

	/*
	Echo.join('chatMessage.1')
	.here((users) => {
		console.log("here");
	})
	.joining((user) => {
		console.log("joining");
	})
	.leaving((user) => {
		console.log("leaving");
	}); */
};

jQuery(document).ready(function() {
	var chatObj = new Chat();
});