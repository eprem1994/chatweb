@extends('layouts.app')

@section('content')
<div class="container">
	@if (Route::has('login'))
		@if (Auth::check())
			<div class="row">
				<div class="col-md-2">
					<label class="button-label">Chat Rooms</label>
				</div>
				<div class="col-md-8 col-md-offset-0">
					<a href="{{ url('/createChatRoom') }}">
						<button type="button" class="btn">Create Chat Room</button>
					</a>
				</div>
			</div>
		@endif
	@endif
	<div class="row">
		@if (count($chatRooms) === 0)
			<div class="col-md-12">
				<span>Not Chat Room Yet</span>
			</div>
			@else
			<div class="chat-rooms-div">
			@foreach ($chatRooms as $chatRoom)
			<a href="{{$chatRoom['roomUrl'] }}">
				<div class="chat-room-div">
					<div class="chat-room-image-wrapper">
						<img src="{{$chatRoom['image']}}" alt="chat room image">
					</div>
					<div class="chat-room-title-wrapper">
						<label>{{$chatRoom['title']}}</label>
					</div>
				</div>
			</a>
			@endforeach
			<div class="chat-rooms-div">
		@endif
	</div>
</div>
@endsection
