@extends('layouts.app')

@section('content')
<div class="container">
	@if (Route::has('login'))
		@if (Auth::check())
			<div class="row">
				<div class="col-md-8 col-md-offset-0">
					<form method="post" action="saveRoom" enctype="multipart/form-data">
						<div class="form-group">
							<label for="chatRoomTitle">Title</label>
							<input type="text" class="form-control" id="chatRoomTitle" name="chatRoomTitle" placeholder="Title" required>
							<small id="emailHelp" class="form-text text-muted">Enter chat room title.</small>
						</div>
						<div class="form-group">
							<label for="chatRoomImage">Image</label>
							<input type="file" class="form-control" id="chatRoomImage" name="chatRoomImage" placeholder="Image" required>
						</div>
						<div class="form-group">
							<input type="checkbox" class="form-check-input" name="showToLoggedInUsers" id="showToLoggedInUsers">
							<label class="form-check-label" for="showToLoggedInUsers">Show to logged in users</label>
						</div>
						<div class="form-group">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<input type="submit" class="btn btn-primary" value="Save">
						</div>
					</form>
				</div>
			</div>
		@endif
	@endif
</div>
@endsection
