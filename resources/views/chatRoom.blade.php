@extends('layouts.app')

@section('content')
<div class="chat-room-content">
	<div class="messg-wrapper">
		<div class="chat-room-header-wrapper">
			<img src="{{ asset($roomImage) }}" alt="chat room image">
			<h2 class="chat-room-title">Room {{$roomTitle}}</h1>
		</div>
		<div class="messages-wrapper">
			@foreach($messageViewData as $viewData)
			<div class="current-message-wrapper">
				<div class="message"><span>{{ $viewData['message'] }}</span></div>
				<div class="message-author-name"><span><b>{{ $viewData['userName'] }}</b></span></div>
			</div>
			
			@endforeach
		</div>
		<div class="write-message-section">
			<div class="message-textarea-wrapper">
				<textarea id="write-message-section" class="form-control"></textarea>
			</div>
			<div class="message-submit-wrapper">
				<button class="send-message btn btn-default" id="send-message" data-room-id="{{$roomId}}" data-user-id="{{ $currentUserId }}" data-user-name="{{$currentUserName}}">Send</button>
			</div>
		</div>
	</div>
</div>
@endsection